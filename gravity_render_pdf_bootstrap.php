<?php
define( 'GF_RENDER_PDF_ADDON_VERSION', '1.0' );

add_action( 'gform_loaded', array( 'GF_Render_PDF_AddOn_Bootstrap', 'load' ), 5 );

class GF_Render_PDF_AddOn_Bootstrap {

    public static function load() {

        if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
            return;
        }

        require_once( 'class-gfrenderpdfaddon.php' );

        GFAddOn::register( 'GFRenderPDFAddOn' );
    }

}

function gf_render_pdf_addon() {
    return GFRenderPDFAddOn::get_instance();
}
?>