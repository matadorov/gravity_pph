<?php
function assocArrayToArrayOfArrayOfString ($assoc) {
  $arrayKeys   = array_keys($assoc);
  $arrayValues = array_values($assoc);
  return array ($arrayKeys, $arrayValues);
}

function gp_makedir($path) {
  return is_dir($path) || mkdir($path);
}

function gp_write_files($data, $names){
	$saved_files = array();
	$upload_dir = wp_upload_dir();
	$files_dir = array(
		'path'=> $upload_dir['basedir']."/gp_files/",
		'url'=> $upload_dir['baseurl']."/gp_files/"
	);
	gp_makedir($files_dir['path']);
	foreach ($data as $format=>$file_data){
		file_put_contents($files_dir['path'].$names[$format], $file_data);
		$saved_files[$format] = array(
			'path' => $files_dir['path'].$names[$format],
			'url' => $files_dir['url'].$names[$format],
		);
	}

	return $saved_files;
}

function gp_render_files($template, $data, $login_data){
	ini_set('soap.wsdl_cache_enabled', 0);

	$soap = new SoapClient("https://api.livedocx.com/1.2/mailmerge.asmx?WSDL");
	$soap->LogIn($login_data);

	$template_data = base64_encode(file_get_contents($template));

	$soap->setLocalTemplate(
		array(
			'template' => $template_data,
			'format'	=> 'docx'
		)
	);
	$soap->SetFieldValues(
    array (
        'fieldValues' => assocArrayToArrayOfArrayOfString($data)
    )
	);
  $soap->SetBlockFieldValues(
    'kyae',
    array(
      assocArrayToArrayOfArrayOfString(['kyae_standard' => 'Some Standard']),
      assocArrayToArrayOfArrayOfString(['kyae_standard' => 'Some Other Standard']),
      assocArrayToArrayOfArrayOfString(['kyae_standard' => 'Some Third Standard']),
    )
  );
	$soap->CreateDocument();

	$pdf = $soap->RetrieveDocument( array( 'format' => 'pdf' ) );
	$files = array();
	$files['pdf'] = base64_decode($pdf->RetrieveDocumentResult);
	$docx = $soap->RetrieveDocument( array( 'format' => 'docx' ) );
	$files['docx'] = base64_decode($docx->RetrieveDocumentResult);
	$soap->logOut();
	unset($soap);
	return $files;
}


/* Fenom Template Engine Init */
add_action('init', 'gp_init_fenom');

global $fenom;
function gp_init_fenom(){
	require_once("lib/Fenom.php");
	Fenom::registerAutoload();

	$plugin_dir = dirname(__FILE__);
	$template_dir = $plugin_dir."/views";
	$template_cache_dir = $plugin_dir."/views/compiled";

	$options = array(
		"force_compile" => true
	);
	global $fenom;
	$fenom = Fenom::factory($template_dir, $template_cache_dir, $options);
}

/* Guess form by id */
function gp_get_form_by_id($id) {
  if (get_option('math-form-id') == $id) return 'math';
  if (get_option('rla-form-id') == $id) return 'rla';
  return false;
}

/* Get KYAE Employability standards text */
function kyae_employability_standards($form_id){
  $form = GFAPI::get_form($form_id);
  $kyae_choices = array_shift(array_filter($form['fields'], function($field){
    return ($field['label'] == 'Employability Standards');
  }))['choices'];
  $kyae_choices_filtered = [];
  foreach ($kyae_choices as $choice) {
    $kyae_choices_filtered[$choice['value']] = $choice['text'];
  }
  return $kyae_choices_filtered;
}

/*Get Selected Employability Standards for entry*/
function get_kyae_for_entry($entry, $form_type){
  if ($form_type=='math') {
    $id = '59';
  } else {
    $id ='66';
  }
  $choices = kyae_employability_standards($entry['form_id']);

  $selected = explode(',', $entry[$id]);

  $formatted = [];
  foreach ($selected as $value) {
    $formatted[] = $choices[$value];
  }
  return $formatted;
};
/* Get CCR Standards for entry */
function get_ccr_standards_for_entry($entry, $form_type){
  if ($form_type == 'math'){
    $ccr_fields_ids = ['11','12','13','14','15'];
  } else {
    $ccr_fields_ids = [
    '39','41','42','43','44',
    '45','46','47','48','49',
    '50','51','52','53','54',
    '55','56','57','58','59',
    '60','61','62','64'];
  };

  foreach ($ccr_fields_ids as $id) {
    if (!empty($entry[$id])) {
      return explode(',', $entry[$id]);
    }
  }
}
/* Get Focus Standards for entry */
function get_focus_standards_for_entry($entry, $form_type){
  if ($form_type == 'math') {
    $focus_standards_ids = range('23', '40');
  } else {
    $focus_standars_ids = [];
  }

  foreach ($focus_standards_ids as $id) {
    if (!empty($entry[$id])) {
      return explode(',', $entry[$id]);
    }
  }
}

/* Get all shortcode data for math */
function get_shortcode_data($entry){
  $form_type = gp_get_form_by_id($entry['form_id']);

  $kyae = get_kyae_for_entry($entry, $form_type);
  $upload_dir = wp_upload_dir();
  $filename = $upload_dir['baseurl'].'/gp_files/entry-'.$entry['id'];
  $ccr = get_ccr_standards_for_entry($entry, $form_type);

  $focus = get_focus_standards_for_entry($entry, $form_type);
  if ($form_type == 'math') {
    return [
      'lesson_title' => $entry['44'],
      'unit_title'  => $entry['2'],
      'nrs_level'   => $entry['5'],
      'length_of_lesson' => $entry['7'],
      'lesson_purpose'    => $entry['67'],
      'related_documents'  => '// To-Do: Related Documents',
      'lesson_objectives' => $entry['69'],
      'student_target'  => $entry['71'],
      'kyae'  => $kyae,
      'mwotl_level'  => $entry['16'],
      'ccr_standards'  => $ccr,
      'focus_standards' => $focus,
      'docx_url'  => $filename.".docx",
      'pdf_url'  => $filename.".docx"
    ];
  } else {
    return [
      'lesson_title' => $entry['2'],
      'unit_title'  => $entry['1'],
      'nrs_level'   => $entry['6'],
      'length_of_lesson' => $entry['8'],
      'lesson_purpose'    => $entry['20'],
      'related_documents'  => '// To-Do: Related Documents',
      'lesson_objectives' => $entry['22'],
      'student_target'  => $entry['24'],
      'kyae'  => $kyae,
      "lead_ccr_anchor_standard" => $entry['38'],
      'mwotl_level'  => $entry['40'],
      'ccr_standards'  => $ccr,
      'focus_standards' => $focus,
      'docx_url'  => $filename.".docx",
      'pdf_url'  => $filename.".docx"
    ];
  };
}
?>
