<?
/*
Plugin Name: Gravity PPH
Plugin URI: https://gitlab.com/matadorov/gravity_pph
Description: Custom View for Gravity Forms entries. DOCX and PDF rendering
Version:     1.0
Author:      Andriy Choroba
Author URI:  http://pph.me/tabun_matadorov '
GitLab Plugin URI: https://gitlab.com/matadorov/gravity_pph
*/

// Form Preview Before Submission
require_once( dirname(__FILE__) . '/gf_preview_confirmation.php');

// Settings Page
require_once( dirname(__FILE__) . '/settings.php'  ); // Settings page

// Helper Functions
require_once( dirname(__FILE__) . '/functions.php' ); // Helper functions

// Gravity Forms addon
require_once( dirname(__FILE__) . '/gravity_render_pdf_bootstrap.php');

// Register entry_id query var for our shortcode

add_filter('query_vars', function($vars){
  $vars[] = 'entry_id';
  return $vars;
});
?>
