<?php 
/** Fenom template 'gf_entry_shortcode.html' compiled at 2016-09-14 06:24:29 */
return new Fenom\Render($fenom, function ($var, $tpl) {
?><style>
	.jp-entry-container {
		display:flex;
		flex-direction:column;
		justify-content:flex-start;
		width:100%;
	}

	.row {
		display:flex;
	}
	.table-wrapper {
		flex:1;
	}
	.jp-entry-lesson-data {
		width:100%;
		border-collapse:0;
		border:0;
		font-size:small;
		margin:0;
	}
	.jp-entry-lesson-data tr:nth-child(odd){
		background:#c6d0da;
	}
	.jp-entry-lesson-data th {
		padding:5px;
		width: 250px;
		text-align:center;
		border-right:2px solid #5f6f81;
		text-align:left;
		padding-left:15px;
	}
	.jp-entry-lesson-data td {
		padding:5px;
		padding-left:15px;
	}

	.jp-table-two tr {
		background:#c6d0da;
	}
	.jp-table-two tr:nth-child(even) {
		background:whitesmoke;
	}
	.jp-table-two th{
		color:black;
		font-size:larger;
		width:250px;
		padding:15px;
		border-right:2px solid #5f6f81;
	}
	.jp-table-two td {
		padding:15px;
	}
	.download-files {
		width:100px;
		text-align:center;
	}
	.download-files a {
		text-decoration:none;
		margin:0 5px;
	}
	.ccr-standards {
		width:50%;

	}
</style>

<section class="jp-entry-container">
	<div class="row">
		<div class="table-wrapper">
			<table class="jp-entry-lesson-data">
				<tr>
					<th>Lesson Title</th>
					<td><?php
/* gf_entry_shortcode.html:74: {$lesson_title} */
 echo $var["lesson_title"]; ?></td>
				</tr>
				<tr>
					<th>Unit Title</th>
					<td><?php
/* gf_entry_shortcode.html:78: {$unit_title} */
 echo $var["unit_title"]; ?></td>
				</tr>
				<tr>
					<th>NRS Level</th>
					<td><?php
/* gf_entry_shortcode.html:82: {$nrs_level} */
 echo $var["nrs_level"]; ?></td>
				</tr>
				<tr>
					<th>Length of Lesson</th>
					<td><?php
/* gf_entry_shortcode.html:86: {$length_of_lesson} */
 echo $var["length_of_lesson"]; ?></td>
				</tr>
			</table>
		</div>
		<span class="download-files">
			<a href="<?php
/* gf_entry_shortcode.html:91: {$docx_url} */
 echo $var["docx_url"]; ?>" class="docx download-file-icon">
				<img src="http://lessons.jeffmoser.com/wp-content/themes/Avada/lib/img/word.png" />
			</a>
			<a href="<?php
/* gf_entry_shortcode.html:94: {$pdf_url} */
 echo $var["pdf_url"]; ?>" class="pdf download-file-icon">
				<img src="http://lessons.jeffmoser.com/wp-content/themes/Avada/lib/img/pdf.png" />
			</a>
		</span>
	</div>
	<div class="row">
		<table class="jp-table-two">
			<tr>
				<th>Lesson Purpose</th>
				<td>
					<?php
/* gf_entry_shortcode.html:104: {$lesson_purpose} */
 echo $var["lesson_purpose"]; ?>
				</td>
			</tr>
			<tr>
				<th>Related Documents</th>
				<td><?php
/* gf_entry_shortcode.html:109: {$related_documents} */
 echo $var["related_documents"]; ?></td>
			</tr>
			<tr>
				<th>Lesson Objectives</th>
				<td><?php
/* gf_entry_shortcode.html:113: {$lesson_objectives} */
 echo $var["lesson_objectives"]; ?></td>
			</tr>
			<tr>
				<th>Student Target</th>
				<td><?php
/* gf_entry_shortcode.html:117: {$student_target} */
 echo $var["student_target"]; ?></td>
			</tr>
		</table>
	</div>
	<div class="row">
		<div class="ccr-standards">
			<h3>CCR Standards</h3>
			<?php
/* gf_entry_shortcode.html:124: {if $lead_ccr_anchor_standard} */
 if($var["lead_ccr_anchor_standard"]) { ?>
			<p><?php
/* gf_entry_shortcode.html:125: {$lead_ccr_anchor_standard} */
 echo $var["lead_ccr_anchor_standard"]; ?></p>
			<?php
/* gf_entry_shortcode.html:126: {/if} */
 } ?>
			<p><?php
/* gf_entry_shortcode.html:127: {$mwotl_level} */
 echo $var["mwotl_level"]; ?></p>
			<h4>Level-Specific Standards</h4>
			<ul>
				<?php  if(!empty($var["ccr_standards"]) && (is_array($var["ccr_standards"]) || $var["ccr_standards"] instanceof \Traversable)) {
  foreach($var["ccr_standards"] as $var["standard"]) { ?>
					<li><?php
/* gf_entry_shortcode.html:131: {$standard} */
 echo $var["standard"]; ?></li>
				<?php
/* gf_entry_shortcode.html:132: {/foreach} */
   } } ?>
			</ul>
			<h3>Focus Standards</h3>
			<ul>
				<?php  if(!empty($var["focus_standards"]) && (is_array($var["focus_standards"]) || $var["focus_standards"] instanceof \Traversable)) {
  foreach($var["focus_standards"] as $var["standard"]) { ?>
					<li><?php
/* gf_entry_shortcode.html:137: {$standard} */
 echo $var["standard"]; ?></li>
				<?php
/* gf_entry_shortcode.html:138: {/foreach} */
   } } ?>
			</ul>
		</div>
		<div class="kyae">
			<h3>KYAE Employability Standards</h3>
			<ul>
			<?php  if(!empty($var["kyae"]) && (is_array($var["kyae"]) || $var["kyae"] instanceof \Traversable)) {
  foreach($var["kyae"] as $var["standard"]) { ?>
				<li><?php
/* gf_entry_shortcode.html:145: {$standard} */
 echo $var["standard"]; ?></li>
			<?php
/* gf_entry_shortcode.html:146: {/foreach} */
   } } ?>
			</ul>
		</div>
	</div>
</section>
<?php
}, array(
	'options' => 256,
	'provider' => false,
	'name' => 'gf_entry_shortcode.html',
	'base_name' => 'gf_entry_shortcode.html',
	'time' => 1473834038,
	'depends' => array (
  0 => 
  array (
    'gf_entry_shortcode.html' => 1473834038,
  ),
),
	'macros' => array(),

        ));
