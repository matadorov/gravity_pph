<?
GFForms::include_addon_framework();

class GFRenderPDFAddOn extends GFAddOn {

    protected $_version = GF_RENDER_PDF_ADDON_VERSION;
    protected $_min_gravityforms_version = '1.9';
    protected $_slug = 'renderpdfaddon';
    protected $_path = 'renderpdfaddon/renderpdfaddon.php';
    protected $_full_path = __FILE__;
    protected $_title = 'Gravity Forms Render PDF Add-On';
    protected $_short_title = 'Render PDF Add-On';

    private static $_instance = null;

    public static function get_instance() {
        if ( self::$_instance == null ) {
            self::$_instance = new static();
        }

        return self::$_instance;
    }

    public function init() {
        parent::init();
        $entry = GFAPI::get_entry(1);
        $data = array();
        $range = range('1','1000');
        foreach ($entry as $key=>$value) {
        	if ((!empty($value)) && (in_array($key, $range))){
        		$data["field_".$key] = $value;
        	}
        }
        static::render($entry, GFAPI::get_form(1));
        add_shortcode('gf_entry', array(GFRenderPDFAddOn, 'gf_entry_shortcode'));
        add_action('gform_after_submission', array(GFRenderPDFAddOn, 'render'), 10, 2);
    }

    public static function gf_entry_shortcode($atts){
      $id = get_query_var('entry_id');
      $id = empty($atts['entry_id']) ? $id : $atts['entry_id'];
      if (!$id) return false;
      $entry = GFAPI::get_entry($id);
      global $fenom;
      return $fenom->fetch('gf_entry_shortcode.html', get_shortcode_data($entry));
    }

    public static function render($entry, $form){
      $is_form = gp_get_form_by_id($form['fields'][0]['formId']);
      if (!$is_form) return false;

      $template_data = $entry;
      $files_data = gp_render_files(
        gp_template($is_form),
        $template_data,[
          'username' => get_option('phplivedocx-username'),
          'password' => get_option('phplivedocx-password')
      ]);

      $files = gp_write_files($files_data, [
        'pdf' => 'entry-'.$entry['id'].'.pdf',
        'docx' => 'entry-'.$entry['id'].'.docx',
      ]);

    }
  }
?>
