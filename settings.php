<?php

function gp_settings_page(){
  add_settings_section("docx-template-section", "DocX Template", null, "gravity-pph");
  add_settings_section('phplivedocx', 'phplivedocx.org login/password', null, 'gravity-pph');
  add_settings_section('gravity_forms_ids', "ID's of Gravity Forms", null, 'gravity-pph');

  add_settings_field("docx-template-math", "Math Form", "template_display_math", "gravity-pph", "docx-template-section");
  add_settings_field("docx-template-rla", 'RLA Form', "template_display_rla", 'gravity-pph', 'docx-template-section');
  add_settings_field("phplivedocx-username", 'Username', "username_display", "gravity-pph", 'phplivedocx');
  add_settings_field("phplivedocx-password", 'Password', 'password_display', 'gravity-pph', 'phplivedocx');
  add_settings_field('math-form-id', "Math Form ID", 'math_form_id_display', 'gravity-pph', 'gravity_forms_ids');
  add_settings_field('rla-form-id', "RLA Form ID", 'rla_form_id_display', 'gravity-pph', 'gravity_forms_ids');

  register_setting("gravity-pph", "phplivedocx-username");
  register_setting("gravity-pph", "phplivedocx-password");
  register_setting("gravity-pph", "docx-template-math", "handle_template_upload_math");
  register_setting("gravity-pph", "docx-template-rla", "handle_template_upload_rla");
  register_setting('gravity-pph', 'math-form-id');
  register_setting('gravity-pph', 'rla-form-id');

}

function handle_template_upload_math($option) {
	if(!empty($_FILES["docx-template-math"]["tmp_name"])){
  	$urls = wp_handle_upload($_FILES["docx-template-math"], array('test_form' => FALSE));
    return json_encode($urls);
  }

  return get_option('docx-template-math');
}

function template_display_math()
{
?>
      <input type="file" name="docx-template-math" />
<?php
	    $template = json_decode( get_option('docx-template-math'), true );
      echo "<a href='{$template['url']}' download>".basename($template['file'])."</a>";
}
function handle_template_upload_rla($option) {
  if(!empty($_FILES["docx-template-rla"]["tmp_name"])){
    $urls = wp_handle_upload($_FILES["docx-template-rla"], array('test_form' => FALSE));
    return json_encode($urls);
  }

  return get_option('docx-template-rla');
}

function template_display_rla()
{
?>
      <input type="file" name="docx-template-rla" />
<?php
      $template = json_decode( get_option('docx-template-rla'), true );
      echo "<a href='{$template['url']}' download>".basename($template['file'])."</a>";
}
function username_display(){
	?>
		<input type="text" value="<?php echo get_option('phplivedocx-username'); ?>" id="phplivedocx-username" name="phplivedocx-username" />
	<?
}
function password_display(){
	?>
	<input type='text' value="<?php echo get_option('phplivedocx-password'); ?>" name="phplivedocx-password"/>
	<?php
}
function math_form_id_display(){
  ?>
  <input type="text" value="<?php echo get_option('math-form-id'); ?>" name="math-form-id" />
  <?php
}
function rla_form_id_display(){
  ?>
  <input type="text" value="<?php echo get_option('rla-form-id'); ?>" name="rla-form-id" />
  <?php
}
add_action("admin_init", "gp_settings_page");

function gp_page()
{
  ?>
      <div class="wrap">
         <h1>Gravity PPH</h1>

         <form method="POST" action="options.php" enctype="multipart/form-data">
            <?php
              settings_fields("gravity-pph");
              do_settings_sections("gravity-pph");
              submit_button();

            ?>
         </form>
      </div>
   <?php
}

function gp_menu_item()
{
  add_submenu_page("options-general.php", "Gravity PPH", "Gravity PPH", "manage_options", "gravity-pph", "gp_page");
}

add_action("admin_menu", "gp_menu_item");


function gp_template($form){
	$template = json_decode(get_option("docx-template-{$form}"));
	return $template->file;
}
?>
